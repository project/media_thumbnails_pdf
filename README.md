# Media Thumbnails PDF module

## INTRODUCTION ##

This module uses the [Media Thumbnails]
(https://www.drupal.org/project/media_thumbnails) framework to create media
entity thumbnails for pdf files.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/media_thumbnails_pdf

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/media_thumbnails_pdf


## REQUIREMENTS ##

This module has dependency on the below contributed module.
 * Media Thumbnails (https://www.drupal.org/project/media_thumbnails):
   Provides a plugin type for custom media entity thumbnails.


## INSTALLATION ##

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION ##

No configuration is needed.


## MAINTAINERS ##

Current maintainers:
 * Boris Böhne (drubb) - https://www.drupal.org/u/drubb
 * Quentin Fahrner (renrhaf) - https://www.drupal.org/u/renrhaf